@extends('layouts.master')

@section('konten')
<div class="mt-3 ml-5 mr-2">
      <div class="card">
            <div class="card-header">
                  <h3 class="card-title">Table Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                  @if (session('success'))
                  <div class="alert alert-success">
                        {{session('success')}}
                  </div>
                  @endif
                  <a class="btn btn-outline-success mb-2" href="/pertanyaan/create">Create new</a>

                  <table class="table table-bordered">
                        <thead>
                              <tr>
                                    <th style="width: 10px">No.</th>
                                    <th>Judul Pertanyaan</th>
                                    <th>Isi</th>
                                    <th style="width: 40px"><i>Action</i></th>
                              </tr>
                        </thead>
                        <tbody>
                              @forelse($pertanyaans as $key => $pertanyaan)
                              <tr>
                                    <td>
                                          {{ $key + 1 }}
                                    </td>
                                    <td>
                                          {{ $pertanyaan->judul }}
                                    </td>
                                    <td>
                                          {{ $pertanyaan->isi }}
                                    </td>
                                    <td style="display:flex">
                                          <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>

                                          <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a>
                                          <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <!-- <input type="submit" class="btn btn-danger btn-sm"> -->
                                                <button class="fas fa-trash btn-danger btn-sm"></button>

                                          </form>
                                          <!-- <div class="btn-group btn-group-sm">
                                                <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                                <a href="#" class="btn btn-default"><i class="fas fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                          </div> -->
                                    </td>
                              </tr>
                              @empty
                              <tr>
                                    <td colspan="4" align="center" class="text-primary">
                                          Tidak ada data.
                                    </td>
                              </tr>
                              @endforelse
                              <!-- <tr>
                                    <td>1.</td>
                                    <td>Update software</td>
                                    <td>
                                          <div class="progress progress-xs">
                                                <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                                          </div>
                                    </td>
                                    <td><span class="badge bg-danger">55%</span></td>
                              </tr> -->
                        </tbody>
                  </table>
            </div>
            <!-- /.card-body -->

      </div>
</div>
@endsection