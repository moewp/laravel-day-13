@extends('layouts.master')

@section('konten')
<div class="ml-5 mt-5 mr-5">
      <div class="card card-primary">
            <div class="card-header">
                  <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                  @csrf
                  @method('PUT')
                  <div class="card-body">
                        <div class="form-group">
                              <label for="judul">Judul Pertanyaan</label>
                              <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaan->judul) }}" placeholder="masukan judul ...">
                              @error('judul')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                        </div>
                        <div class="form-group">
                              <label for="isi">Isi Pertanyaan</label>
                              <!-- <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $pertanyaan->isi) }}" placeholder="masukan isi pertanyaan..."> -->
                              <textarea class="form-control" rows="3" id="isi" name="isi" style="margin-top: 0px; margin-bottom: 0px; height: 124px;">{{ old('isi', $pertanyaan->isi) }}</textarea>
                              @error('isi')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                        </div>
                  </div>
                  <!-- /.card-body -->
                  <!-- <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-right">
                              <li class="page-item"><a class="page-link" href="#">«</a></li>
                              <li class="page-item"><a class="page-link" href="#">1</a></li>
                              <li class="page-item"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#">»</a></li>
                        </ul>
                  </div> -->
                  <div class="card-footer d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </form>
      </div>
</div>
@endsection