@extends('layouts.master')

@section('data-tables')
<div class="card">
      <div class="card-header">
            <h3 class="card-title">DataTable Laravel Coy</h3>
      </div>
      <!-- /.card-header -->
      @include('layouts.partials.card-data')
      <!-- /.card-body -->
</div>
@endsection

@section('skrip')
<!-- JQuery -->
<script src="{{asset('/plugins/datatables/jquery.dataTables.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endsection

@push('data')
<script>
      $(function() {
            $("#example1").DataTable();
      });
</script>
@endpush