<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan; //memanggil model Eloquent

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('crud.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'judul' => 'required|unique:pertanyaan',
                'isi' => 'required'
            ]
        );

        // $query = DB::table('pertanyaan')->insert(
        //     [
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]
        // ); imi dibuat Query Builder

        // $pertanyaana = new Pertanyaan; //$pertanyaana adalah objek baru dari model. model ini sebuah class, maka dibuat objek dulu terus masukin nilai22nya
        // $pertanyaana->judul = $request["judul"];
        // $pertanyaana->isi = $request["isi"];
        // $pertanyaana->save(); //mirip INSERT INTO pertanyaan (judul, isi) VALUES ... .ini pake Eloquent

        //mass asignment
        $pertanyaang = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Yeayy Pertanyaanmu Berhasil Disimpan...');
    }

    public function index()
    {
        // $pertanyaans = DB::table('pertanyaan')->get(); //seperti SELECT *FROM pertantaan ((di MySQL)) pake Query Builder
        // dd($pertanyaans);
        $pertanyaans = Pertanyaan::all(); //pake Eloquent
        return view('crud.index', compact('pertanyaans'));
    }

    public function show($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first(); //pake Query Builder
        // dd($pertanyaan);
        $pertanyaan = Pertanyaan::find($id); //pake Eloquent
        return view('crud.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first(); //pake Query Builder
        $pertanyaan = Pertanyaan::find($id); //pake Eloquent
        return view('crud.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
            [
                'judul' => 'required|unique:pertanyaan',
                'isi' => 'required'
            ]
        );

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi']
        //     ]); //pake Query Builder

        $update = Pertanyaan::where('id', $id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]); //pake Eloquent

        return redirect('/pertanyaan')->with('success', 'Uhuyy Pertanyaanmu Berhasil Diubah...');
    }

    public function delete($id)
    {
        // DB::table('pertanyaan')->where('id', $id)->delete(); //pake Query Builder
        Pertanyaan::destroy($id); //pake Eloquent
        return redirect('/pertanyaan')->with('success', 'Cuy Pertanyaanmu Berhasil Dihapus...');
    }

    public function imageUpload()
    {
        return view('imageUpload');
    }

    public function imageUploadPost(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time() . '.' . $request->image->extension();

        $request->image->move(public_path('images'), $imageName);

        return back()
            ->with('success', 'You have successfully upload image.')
            ->with('image', $imageName);
    }
}
