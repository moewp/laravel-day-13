<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan"; //deklarasi nama tabel di DB buat ELoquent

    protected $fillable = ["judul", "isi"]; //mass assignment -white list

    // protected $guarded = []; //mass assignment black list
}
